import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';

createReactinterceptor();

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);


function createReactinterceptor () {
  axios.interceptors.request.use((config) => {
    config.baseURL = 'http://localhost:8080/'
    return config
  })
}