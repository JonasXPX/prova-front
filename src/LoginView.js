import { Button, Grid, Input } from "@material-ui/core"
import axios from "axios"
import { useState } from "react"

export default function LoginView ({ onLogin }) {
  
    const [form, setForm] = useState({
      username: '',
      password: '',
      messageSignin: ''
    })
  
    const login = async () => {
      const { data } = await axios.post('login', {
        email: form.username,
        password: form.password
      })
      onLogin(data)
    }

    const signin = async () => {
      try {
        const { status } = await axios.put('user', {
          email: form.username,
          password: form.password
        })
        
        if(status === 201) {
          defineMessage('messageSignin','Conta criada com sucesso!')
        }
      } catch (err) {
        defineMessage('messageSignin','Erro ao criar a conta')
      }
    }

    const defineMessage = (field, message) => {
      setForm({
        ...form,
        [field]: message
      })
    }
  
    const handlerChange = (field) => event => {
      setForm({
        ...form,
        [field]: event.target.value
      })
    }
  
    return (
      <Grid container>
        <Grid item xs={6}>
          <form onSubmit={login} className='p-1'>
            <Grid container spacing={3} alignContent='center'>
              <Grid Grid item xs={12}>
                <Input 
                  name='username'
                  placeholder='email'
                  onChange={handlerChange('username')}
                />
              </Grid>
              <Grid item xs={12}>
                <Input 
                  name='password'
                  placeholder='senha'
                  type='password'
                  onChange={handlerChange('password')}
                />
              </Grid>
              <Grid item xs={12}>
                <Button onClick={login}>Login</Button>
                {form.messageLogin && <p>
                  {form.messageLogin}
                </p>}
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={6}>
          <form className='p-1'>
            <Grid container spacing={3} alignContent='center'>
              <Grid Grid item xs={12}>
                <Input 
                  name='username'
                  placeholder='email'
                  onChange={handlerChange('username')}
                />
              </Grid>
              <Grid item xs={12}>
                <Input 
                  name='password'
                  placeholder='senha'
                  type='password'
                  onChange={handlerChange('password')}
                />
              </Grid>
              <Grid item xs={12}>
                <Button onClick={signin}>Cadastrar</Button>
                {form.messageSignin && <p>
                  {form.messageSignin}
                </p>}
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    )
  }