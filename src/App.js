import axios from 'axios';
import { useEffect, useState } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import './App.css';
import LoginView from './LoginView'
import ContactView from './ContactView'

function App(props) {
  const [isLogin, setIsLogin] = useState(false)
  const [token, setToken] = useState('')
  const { push } = useHistory()

  useEffect(() => {
    if(!isLogin) {
      push('/login')
    }
  }, [])

  const handlerLogin = token => {
    setToken(token)
    setIsLogin(true)
    console.log(props)
    registerInterceptor(token, push)
    push('/app')
  }

  return (
    <Switch>
      <Route exact path={['/', '/login']}>
        <LoginView onLogin={handlerLogin.bind(this)} />
      </Route>

      <Route path='/app'>
        {isLogin && (
          <div>
            <ContactView />
          </div>
        )}
      </Route>
    </Switch>
  );
}

export default App;

function registerInterceptor(token, push) {
  axios.interceptors.request.use(config => {
    config.headers['Authorization'] = 'Bearer ' + token
    return config
  })
  axios.interceptors.response.use(config => {
    if(config.status === 403) {
      push('/login')
      return config
    }
    return config
  })
}