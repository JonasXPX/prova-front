import { Button, Dialog, Grid, IconButton, List, ListItem, Typography } from "@material-ui/core"
import { Fragment, useEffect, useState } from "react"
import DeleteIcon from '@material-ui/icons/Delete'
import CreateIcon from '@material-ui/icons/Create'
import axios from 'axios'
import ContactEditDialog from './ContactEditDialog'


export default function ContactView () {
    const [contacts, setContacts] = useState([])
    const [dialogOpen, setDialogOpen] = useState(false)
    const [editContact, setEditContact] = useState({})
  
    useEffect(() => {
      getContacts()
        .then(result => setContacts(result.content))
    }, [])
  
    const onSaveContact = async (contact) => {
      const { data, status } = await axios.post('/contact', contact)
      if(status === 200) {
        if(contact.id) {
          updateFromIndex(contact)
        } else {
          insertNewContact(data)
        }
        handlerCloseDialog()
      }
    }
  
    const handlerCloseDialog = () => {
      setDialogOpen(false)
    }
    
    const openDialog = () => {
      setDialogOpen(true)
    }
  
    const newContact = () => {
      setEditContact({})
      openDialog(true)
    }
  
    const handlerEditContact = contact => {
      setEditContact(contact)
      openDialog()
    }
  
    const handlerDeleteContact = async contact => {
      const { status } = await axios.delete(`/contact/${contact.id}`)
      if(status === 200) {
        removeFromContacts(contact.id)
      }
    }
    
    const updateFromIndex = contact => {
      const copyOfArray = [...contacts]
      const index = copyOfArray.findIndex(contactItem => contactItem.id === contact.id)
      copyOfArray.splice(index, 1, contact)
      setContacts(copyOfArray)
    }
  
    const insertNewContact = contact => {
      const copyOfArray = [...contacts, contact]
      setContacts(copyOfArray)
    }
  
    const removeFromContacts = id => {
      const copyOfArray = [...contacts]
      const index = copyOfArray.findIndex(contactItem => contactItem.id === id)
      copyOfArray.splice(index, 1)
      setContacts(copyOfArray)
    }
  
    return (
      <div className='p-1'>
        <Button
          variant='contained'
          color='primary'
          onClick={newContact}>
          Novo contato
        </Button>
        <Dialog onClose={handlerCloseDialog} open={dialogOpen}>
          <ContactEditDialog onSave={onSaveContact} contact={editContact} />
        </Dialog>
        <List>
          {contacts.map(item => (
            <Fragment key={item.id}>
              <ListItem>
                <Grid container>
                  <Grid item xs={12} className='border p-1'>
                    <Typography variant='h5'>{item.name}</Typography>
                    <Grid container alignItems='center'>
                      <Grid item xs={5}>
                        <Typography variant='body2'>{item.email}</Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant='body2'>{item.celphone}</Typography>
                      </Grid>
                      <Grid item xs={1}>
                        <IconButton onClick={() => handlerEditContact(item)}>
                          <CreateIcon />
                        </IconButton>
                        <IconButton onClick={() => handlerDeleteContact(item)}>
                          <DeleteIcon />
                        </IconButton>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </ListItem>
            </Fragment>
          ))}
        </List>
        
      </div>
    )
  
  }


async function getContacts() {
    try {
        const { data } = await axios.get('/contact')
        return data
    } catch(err) {
        console.log(err)
    }
    return null
}
  