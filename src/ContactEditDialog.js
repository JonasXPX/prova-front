import { Button, Grid, Input, Typography } from "@material-ui/core"
import { useState } from "react"

export default function ContactEditView({ contact, onSave }) {
    const [state, setState] = useState({
      name: contact.name,
      email: contact.email,
      celphone: contact.celphone,
      id: contact.id
    })
  
    const handlerValueChange = field => event => setState({...state, [field]: event.target.value})
  
    const handlerSave = async () => {
      await onSave(state)
    }
  
  
    return (
      <div className='p-1'>
        <Typography variant='h5'>Contato</Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Input 
              value={state.name}
              placeholder='Nome'
              onChange={handlerValueChange('name')}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <Input 
              value={state.email}
              placeholder='Email'
              onChange={handlerValueChange('email')}
              fullWidth
            />  
          </Grid>
          <Grid item xs={12}>
            <Input 
              value={state.celphone}
              placeholder='Telefone'
              onChange={handlerValueChange('celphone')}
              fullWidth
            />
          </Grid>
          <Grid item xs={6}>
            <Button
              variant='contained'
              onClick={handlerSave}>
              Salvar
            </Button>
          </Grid>
        </Grid>
      
      </div>
    )
  }
  